from tkinter import filedialog
from tkinter import *
from shutil import copy2
from PIL import ImageTk, Image
import os
import threading
import time

def file_name(T):
    x = filedialog.askopenfilename(initialdir="/home/",
                                   title="Select file", filetypes=(("python files", "*.py"),
                                                                   ("all files", "*.*")))
    desp.config(text='', fg='red')
    T.config(state=NORMAL)
    T.delete('1.0', END)
    T.insert(INSERT, x)
    T.config(state=DISABLED)


def save_file(T, file, u):
    if T.get("1.0", "end-1c") == "":
        desp.config(text='Please select the python file for feature extraction', fg='red')
        return ""
    copy2(T.get("1.0", "end-1c"), file+"/")
    T.config(state=NORMAL)
    T.delete('1.0', END)
    T.config(state=DISABLED)
    u.flash()
    desp.config(text='Upload Completed for feature extraction', fg='green')

def s_save_file():
    if mb_T.get("1.0", "end-1c") == "" or me_T.get("1.0", "end-1c") == "":
        desp.config(text='Please select both python file for'
                         ' model building and model evaluation', fg='red')
        return ""
    copy2(mb_T.get("1.0", "end-1c"), "model_building/")
    copy2(me_T.get("1.0", "end-1c"), "model_evaluation/")
    mb_T.config(state=NORMAL)
    mb_T.delete('1.0', END)
    mb_T.config(state=DISABLED)
    me_T.config(state=NORMAL)
    me_T.delete('1.0', END)
    me_T.config(state=DISABLED)
    me_u.flash()
    desp.config(text='Upload Completed for model building and model evaluation', fg='green')

def page1():
    count.set(1)
    page2text.grid_forget()
    fe1.grid_forget()
    mb1.grid_forget()
    me1.grid_forget()
    fe_l.grid_forget()
    mb_l.grid_forget()
    me_l.grid_forget()
    fe_T.grid(row=1, column=1)
    mb_T.grid(row=4, column=1)
    me_T.grid(row=7, column=1)
    fe.grid(row=1, column=2)
    mb.grid(row=4, column=2)
    me.grid(row=7, column=2)
    fe_u.grid(row=2, columnspan=3)
    me_u.grid(row=8, columnspan=3)
    fe_l.grid(row=1, column=0, sticky=W)
    mb_l.grid(row=4, column=0, sticky=W)
    me_l.grid(row=7, column=0, sticky=W)
    desp.grid(row=10, columnspan=3, sticky=W)
    fe_l.config(fg=p1color)
    mb_l.config(fg=p1color)
    me_l.config(fg=p1color)
    space.grid(row=3)
    refresh.grid_forget()
    refresh1.grid_forget()
    page1btn.config(fg='black')
    page2btn.config(fg='grey')
    page3btn.config(fg='grey')

def page2():
    count.set(2)
    fe_T.grid_forget()
    mb_T.grid_forget()
    me_T.grid_forget()
    space.grid_forget()
    fe.grid_forget()
    mb.grid_forget()
    me.grid_forget()
    fe_u.grid_forget()
    me_u.grid_forget()
    fe_l.grid_forget()
    mb_l.grid_forget()
    me_l.grid_forget()
    fe1.grid_forget()
    mb1.grid_forget()
    me1.grid_forget()
    desp.config(text='', fg='green')
    desp.grid_forget()
    refresh1.grid_forget()
    if os.path.exists("data/None.png"):
        temp = ImageTk.PhotoImage(Image.open("data/None.png"))
        page2text.config(image=temp)
        page2text.Image = temp
    else:
        page2text.config(image='', text="Wait for some time", background=bgcolor)
    page2text.grid(row=1, columnspan=3)
    refresh.grid(row=2,sticky=W)
    page1btn.config(fg='grey')
    page2btn.config(fg='black')
    page3btn.config(fg='grey')

def page3():
    count.set(3)
    fe_path = [os.path.join(f[:-3]) for f in os.listdir(feature_extraction_folder) if (f[-3:] == '.py')]
    mb_path = [os.path.join(f[:-3]) for f in os.listdir(model_building_folder) if (f[-3:] == '.py')]
    me_path = [os.path.join(f[:-3]) for f in os.listdir(model_evaluation_folder) if (f[-3:] == '.py')]
    fe_T.grid_forget()
    mb_T.grid_forget()
    me_T.grid_forget()
    space.grid_forget()
    fe.grid_forget()
    mb.grid_forget()
    me.grid_forget()
    fe_u.grid_forget()
    me_u.grid_forget()
    refresh.grid_forget()
    desp.grid_forget()
    desp.config(text='', fg='green')
    refresh1.grid(row=3, sticky=W)
    fe_l.grid(row=1, column=0)
    mb_l.grid(row=1, column=1)
    me_l.grid(row=1, column=2)
    fe1.config(state=NORMAL)
    fe1.delete('1.0', END)
    for x in fe_path:
        fe1.insert(INSERT, x)
        fe1.insert(INSERT, '\n')
    fe1.config(state=DISABLED)
    fe1.grid(row=2, column=0)
    mb1.config(state=NORMAL)
    mb1.delete('1.0', END)
    for x in mb_path:
        mb1.insert(INSERT, x)
        mb1.insert(INSERT, '\n')
    mb1.config(state=DISABLED)
    mb1.grid(row=2, column=1)
    me1.config(state=NORMAL)
    me1.delete('1.0', END)
    for x in me_path:
        me1.insert(INSERT, x)
        me1.insert(INSERT, '\n')
    me1.config(state=DISABLED)
    me1.grid(row=2, column=2)
    fe_l.config(background=bgcolor, fg=p3color)
    mb_l.config(background=bgcolor, fg=p3color)
    me_l.config(background=bgcolor, fg=p3color)
    fe1.config(background='white')
    mb1.config(background='white')
    me1.config(background='white')
    page2text.grid_forget()
    page1btn.config(fg='grey')
    page2btn.config(fg='grey')
    page3btn.config(fg='black')

def rfh():
    while True:
        if count.get() == 1:
            root.after(1000, page1)
        elif count.get() == 2:
            page2()
        elif count.get() == 3:
            root.after(1000, page3)
        time.sleep(10)


model_evaluation_folder = 'model_evaluation'
model_building_folder = 'model_building'
feature_extraction_folder = 'feature_extraction'


main = Tk()
count = IntVar()
count.set(1)
bgcolor = 'sky blue'
p1color = 'blue'
p3color = 'brown4'
tcolor = 'light sky blue'
root = Frame(main)
root1 = Frame(main)
despF = Frame(main)
root1.config(background=bgcolor)

page2text = Label(root, background=bgcolor)
if os.path.exists("/hdata/None.png"):
    img = ImageTk.PhotoImage(Image.open("data/None.png"))
    page2text.config(image=img)
else:
    page2text.config(text="Wait for some time", background=bgcolor, font=32)


page1btn = Button(root1, text="Adding Feature", command=lambda: page1())
page2btn = Button(root1, text="Graph Representation", command=lambda: page2())
page3btn = Button(root1, text="File available", command=lambda: page3())
page1btn.config(background='white', fg='black')
page2btn.config(background='white', fg='grey')
page3btn.config(background='white', fg='grey')
root1.pack(side=TOP,fill=X)
root.pack(side=TOP, expand=YES)
root.config(background=bgcolor)

page1btn.grid(row=0,column=0)
page2btn.grid(row=0,column=1)
page3btn.grid(row=0,column=2)
fe_l = Label(root, font='Helvetica 16 bold', text='Feature Extraction', fg=p1color)
fe_l.config(background=bgcolor)
mb_l =Label(root, font='Helvetica 16 bold', text='Model Building', fg=p1color)
mb_l.config(background=bgcolor)
me_l =Label(root, font='Helvetica 16 bold', text='Model Evaluation', fg=p1color)
me_l.config(background=bgcolor)
space = Label(root, height=3, background=bgcolor)
space.grid(row=3)
fe_l.grid(row=0, column=0, sticky=W)
mb_l.grid(row=5, column=0, sticky=W)
me_l.grid(row=8, column=0, sticky=W)
fe_T = Text(root, font=32, height=2, width=30)
mb_T = Text(root, font=32, height=2, width=30)
me_T = Text(root, font=32, height=2, width=30)
fe_T.config(state=DISABLED, background='white')
mb_T.config(state=DISABLED, background='white')
me_T.config(state=DISABLED, background='white')
fe_T.grid(row=0, column=1)
mb_T.grid(row=5, column=1)
me_T.grid(row=8, column=1)
fe = Button(root, font=32, text='Select', width=25, height=2, fg=p1color, command=lambda: file_name(fe_T))
mb = Button(root, font=32, text='Select', width=25, height=2,fg=p1color, command=lambda: file_name(mb_T))
me = Button(root, font=32, text='Select', width=25, height=2,fg=p1color, command=lambda: file_name(me_T))
fe.config(background='white')
mb.config(background='white')
me.config(background='white')
fe.grid(row=0, column=2)
mb.grid(row=5, column=2)
me.grid(row=8, column=2)
fe_u = Button(root, font=32, text='Upload', width=74, fg=p1color,
              command=lambda: save_file(fe_T, feature_extraction_folder, fe_u))
me_u = Button(root, font=32, text='Upload', width=74,fg=p1color, command=lambda: s_save_file())
fe_u.config(background='white')
me_u.config(background='white')
fe_u.grid(row=1, columnspan=3)
me_u.grid(row=9, columnspan=3)
fe1 = Text(root, font=32, height=20, width=30)
mb1 = Text(root, font=32, height=20, width=30)
me1 = Text(root, font=32, height=20, width=30)
fe1.config(state=DISABLED)
mb1.config(state=DISABLED)
me1.config(state=DISABLED)
refresh = Button(root, text="Refresh", command=lambda: page2())
refresh1 = Button(root, text="Refresh", command=lambda: page3())
refresh.config(background='white', fg='black')
refresh1.config(background='white', fg=p3color)
desp = Label(root, background=bgcolor, fg='green')
desp.grid(row=10, columnspan=3, sticky=W)
main.wm_geometry("1600x800")
main.config(background=bgcolor)
t1 = threading.Thread(target=rfh)
t1.start()
main.mainloop()
