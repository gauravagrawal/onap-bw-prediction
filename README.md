Generic Bandwidth prediction application

Before running the application, there are the following packages with the 
the following version is required.

python      3.6.8
tensorflow  1.14.0
numpy       1.17.3
keras       2.2.4
matplotlib  3.0.3
pandas      0.25.1
sklearn     0.21.3
urllib3     1.25.5

The following steps have been followed in order to run the application.

Step1: Run server.py in order to send the data into the DMaaP
Step2: Run the train_csv.py to receive the data from DMaaP then train the model
       from those data and send back to DMaaP.
Step3: Run demo_ui.py for the GUI interface to see the progress of train_csv.py
       file.

In GUI, adding of the various python file like feature extraction file, model 
building file and model evaluation file are possible and those files can also be 
seen in GUI. The graph for the bandwidth prediction has also been shown in the 
same GUI under the different tab.
