# coding: utf-8
from __future__ import print_function
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import tensorflow as tf
import threading
import requests as req
import json
import time
import uuid
import os
import config
from main1 import main1
import numpy as np
from tensorflow.python.platform import tf_logging as logging

DMAAP_HOST_IP = "159.138.56.28"
FILE_NAME = config.ptn + '/period_trend.csv'
os.system("rm -rf data/*.png")

tpId = None
node = None
serviceName = None

VES_EVENT_DATA = \
    {
        "event":{
            "commonEventHeader":{
                "sourceId":"site-214",
                "startEpochMicrosec":1547542547667,
                "eventId": "eventId",
                "nfcNamingCode":"",
                "reportingEntityId":"amzn1.ask.OT4XUODCLXVGPKDMK",
                "internalHeaderFields":
                {
                    "collectorTimeStamp":"Tue, 01 15 2019 08:55:48 UTC"
                },
                "eventType":"bandwidth_notification",
                "priority":"High","version":3,
                "reportingEntityName":"china mobile booth","sequence":960,"domain":"fault",
                "lastEpochMicrosec":1547542547667,
                "eventName":"traffic-congestion",
                "sourceName":"bwPrediction",
                "nfNamingCode":""
            },
            "faultFields":{
                "eventSeverity":"CRITICAL",
                "alarmCondition":"The bandwidth is too low or high","faultFieldsVersion":2,
                "eventCategory":"bandwidth","specificProblem":"The bandwidth is too low or high",
                "alarmInterfaceA":"VNF_194.15.13.138",
                "alarmAdditionalInformation":[{
                    "name": "networkId",
                    "value": "2a77faf5-21f1-4f3f-bad1-8c7273056cac"
                }, {
                    "name": "node",
                    "value": "10.10.10.10"
                }, {
                    "name": "tp-id",
                    "value": "139"
                }, {
                    "name":"bandwidth",
                    "value":"bandwidth"
                }],
                "eventSourceType":"HuaweiBandwidth",
                "vfStatus":"Active"
            }
        }
    }

file = open(FILE_NAME, "w")
class DmaapData:
    def __init__(self):
        self._getDMaaPData()

    def _convertFaultFieldsJsonToEvent(self, faultFieldsJson):
        global serviceName
        global node
        global tpId
        if faultFieldsJson["alarmAdditionalInformation"] is not None:
            file = open(FILE_NAME, "a")
            alarmAdditionalInformation = faultFieldsJson["alarmAdditionalInformation"]
            timestamp = None
            bandwidth = None
            for i in alarmAdditionalInformation:
                if i["name"] == "timestamp":
                    timestamp = i["value"]
                if i["name"] == "bandwidth":
                    bandwidth = i["value"]
                    bandwidth = bandwidth
                if i["name"] == "tp-id":
                    tpId = i["value"]
                if i["name"] == "node":
                    node = i["value"]
                if i["name"] == "serviceName":
                    serviceName = i["value"]
            if timestamp is not None and bandwidth is not None:
                file.write(str(timestamp) + "," + str(bandwidth))
                file.write("\n")
                file.close()

    def _convert_json_to_ves_event(self, entity):
        ves = json.loads(entity)
        faultFieldsJson = ves["event"]["faultFields"]
        self._convertFaultFieldsJsonToEvent(faultFieldsJson)

    def _getDMaaPData(self, ):
        threading.Timer(30, self._getDMaaPData).start()
        resp = req.get("http://" + DMAAP_HOST_IP + ":30227/events/unauthenticated.SEC_OTHER_OUTPUT/g1/c1")
        data = json.loads(resp.text)
        print("Queried dmaap data", data)
        for entity in data:
            self._convert_json_to_ves_event(entity)


def main(_):
    dmaapData = DmaapData()
    dmaapData.__init__()

    while True:
        try:
            #new logic
            csv_file_name = FILE_NAME
            raw_data = np.genfromtxt(csv_file_name, delimiter=',')[:, 1].reshape(-1)
            while len(raw_data) < 300:
                x = int(300 - len(raw_data))
                print("Wait for ", x, " Seconds.")
                time.sleep(10)
                raw_data = np.genfromtxt(csv_file_name, delimiter=',')[:, 1].reshape(-1)
            pre_data = np.genfromtxt(csv_file_name, delimiter=',')
            raw_data = pre_data[:, 1].reshape(-1)
            times = pre_data[:, 0].reshape(-1)
            bandwidth, optimal_mb = main1(raw_data, future_value=config.future_value)
            for i in range(len(bandwidth)):
                cur_time = time.time()
                bandwidthVal = str(float(bandwidth[i]))
                logging.info("updated bandwidth value is: {}".format(str(bandwidthVal)))
                ves_event = VES_EVENT_DATA
                start_time = cur_time * 1000.0
                ves_event["event"]["commonEventHeader"]["eventId"] = str(uuid.uuid4())
                ves_event["event"]["commonEventHeader"]["startEpochMicrosec"] = start_time
                ves_event["event"]["commonEventHeader"]["lastEpochMicrosec"] = start_time

                for i in ves_event["event"]["faultFields"]["alarmAdditionalInformation"]:
                    if i["name"] == "bandwidth":
                        i["value"] = bandwidthVal
                    if i["name"] == "tp-id":
                        i["value"] = tpId
                    if i["name"] == "node":
                        i["value"] = node
                # send ves event to ONAP in json
                event = json.dumps(ves_event).encode()
                headers = {'content-type': 'application/json'}
                r = req.post("http://" + DMAAP_HOST_IP + ":30227/events/unauthenticated.TRAFFIC_CONGESTION",
                             data=event, headers= headers)
                logging.info("response return value {} and reason is {}".format(r.status_code, r.reason))
                logging.info("Sent data to ONAP successfully")


            #calculation for graph
            train_size = int((len(times) - config.lookahead)*config.train_size) + config.lookahead
            val_size = train_size + int((len(times) - config.lookahead)*config.val_size)
            temp_str = "Bandwidth Prediction : " + optimal_mb
            #graph
            plt.figure(figsize=(15, 5))
            plt.title(temp_str)
            plt.axvline(train_size, linestyle="dotted", linewidth=4, color='g')
            plt.axvline(val_size, linestyle="dotted", linewidth=4, color='r')
            plt.axvline(len(times), linestyle="dotted", linewidth=4, color='b')
            plt.plot(times[:train_size], raw_data[:train_size], label='actual', color="g")
            plt.plot(times[train_size:val_size], raw_data[train_size:val_size], label='validation', color="r")
            plt.plot(times[val_size:len(times)], raw_data[val_size:], label='evaluation', color="b")
            plt.plot(np.arange(int(times[-1]+1),
                               int(times[-1]+1+config.future_value)),
                     bandwidth, label='prediction', color="k")
            plt.xlabel('Timestamp')
            plt.ylabel('Bandwidth')
            plt.legend(loc="upper left")
            mycmd = 'chmod -R 777 data'
            os.system(mycmd)
            plt.savefig("data/" + str(serviceName) + ".png")
            time.sleep(30)
        except Exception:
            logging.info("exception occurred")


if __name__ == '__main__':
    tf.logging.set_verbosity(tf.logging.INFO)
    tf.app.run()
