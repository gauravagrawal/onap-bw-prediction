import sys
sys.path.append("..")

import numpy as np
np.random.seed(1)
from tensorflow import set_random_seed
set_random_seed(2)
from model_evaluation import ann_model
from keras.layers import Dense
from keras.callbacks import EarlyStopping
from keras.models import Sequential, model_from_json
import config
import matplotlib.pyplot as plt

def model(x, y, data):
    # Data preparation
    model_file_name = config.model_generated_folder + "/" +\
                      config.file_name[:-4] + "_ann_model.json"
    parameter_file_name = config.model_generated_folder + "/" +\
                          config.file_name[:-4] + "_ann_model.h5"
    size = int(config.train_size * x.shape[0])
    v_size = size + int(config.val_size * x.shape[0])
    x_train = x[:size, :]
    y_train = y[:size]
    x_val = x[size:v_size, :]
    y_val = y[size:v_size]
    x_test = x[v_size:, :]
    y_test = y[v_size:]

    # Data pre-processing
    x_train = x_train.reshape(x_train.shape[0], x_train.shape[1])
    x_val = x_val.reshape(x_val.shape[0], x_val.shape[1])
    x_test = x_test.reshape(x_test.shape[0], x_test.shape[1])

    # define ANN model
    model = Sequential()
    model.add(Dense(64, activation='relu', input_dim=config.lookahead))
    model.add(Dense(32, activation='relu'))
    model.add(Dense(16, activation='relu'))
    model.add(Dense(1))

    #model compilation
    model.compile(loss='mse', optimizer='adam', metrics=['mse', 'mape', 'msle'])
    es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=50)

    #model fitting
    model.fit(x_train, y_train,
              batch_size=64, epochs=1000,
              validation_data=(x_val, y_val), callbacks=[es])

    # serialize model to JSON
    model_json = model.to_json()
    with open(model_file_name, "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights(parameter_file_name)
    print("Saved model to disk")

    pred = ann_model.model_evl(model, x_test)

    if (config.need_graph_in_training):
        # plotting the graph
        plt.figure(figsize=(15, 5))
        plt.title("ANN based model")
        plt.axvline(size + config.lookahead, linestyle="dotted", linewidth=4, color='g')
        plt.axvline(v_size + config.lookahead, linestyle="dotted", linewidth=4, color='r')
        observed_lines = plt.plot(np.arange(size + config.lookahead),
                                  data[:size + config.lookahead], label="observation", color="k")
        evaluated_lines = plt.plot(np.arange(size + config.lookahead, v_size + config.lookahead),
                                   data[size + config.lookahead:v_size + config.lookahead],
                                   label="evaluation", color="g")
        predicted_lines = plt.plot(np.arange(v_size + config.lookahead, len(data)), pred,
                                   label="prediction", color="r")
        plt.legend(handles=[observed_lines[0], evaluated_lines[0], predicted_lines[0]],
                   loc="upper left")
        plt.show()

    return float(np.square(np.subtract(y_test,pred)).mean())

def get_value(data, future_value):
    model_file_name = config.model_generated_folder + "/" + \
                      config.file_name[:-4] + "_ann_model.json"
    parameter_file_name = config.model_generated_folder + "/" + \
                          config.file_name[:-4] + "_ann_model.h5"
    # load json and create model
    json_file = open(model_file_name, 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = model_from_json(loaded_model_json)
    # load weights into new model
    model.load_weights(parameter_file_name)
    print("Loaded model from disk")

    # data processing
    test_val = data[-config.lookahead:].reshape(1, config.lookahead)

    # prediction
    pred = ann_model.future_val(model, test_val, future_value)

    if (config.graph_required):
        # plotting the graph
        plt.figure(figsize=(15, 5))
        plt.title("ANN based model")
        plt.axvline(len(data), linestyle="dotted", linewidth=4, color='g')
        observed_lines = plt.plot(np.arange(len(data)),
                                  data, label="observation", color="k")
        predicted_lines = plt.plot(np.arange(len(data), len(data)+future_value), pred,
                                   label="prediction", color="r")
        plt.legend(handles=[observed_lines[0], predicted_lines[0]],
                   loc="upper left")
        plt.show()

    return pred