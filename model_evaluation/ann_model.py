import numpy as np


def model_evl(model, x_test):
    #data pre-processing
    test_val = np.array(x_test[0]).reshape(1, x_test.shape[1])

    # predicting the model
    pred = np.zeros((x_test.shape[0]))
    ini = test_val.reshape((-1))
    ini = ini[1:]
    for x in range(x_test.shape[0]):
        p = model.predict(test_val)
        p = p.reshape((1))
        test_val = np.append(ini, p).reshape(1, test_val.shape[1])
        ini = test_val.reshape((-1))
        ini = ini[1:]
        pred[x] = p.reshape((1))

    return pred

def future_val(model, test_val, future_value = 1):
    # predicting the model
    pred = np.zeros((future_value))
    ini = test_val.reshape((-1))
    ini = ini[1:]
    for x in range(future_value):
        p = model.predict(test_val)
        p = p.reshape((1))
        test_val = np.append(ini, p).reshape(1, test_val.shape[1])
        ini = test_val.reshape((-1))
        ini = ini[1:]
        pred[x] = p.reshape((1))

    return pred