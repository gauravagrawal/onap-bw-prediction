import numpy as np
import time
import urllib3 as url
import json

i = np.genfromtxt("period_trend.csv", delimiter=',')[:, 0].reshape(-1)
j = np.genfromtxt("period_trend.csv", delimiter=',')[:, 1].reshape(-1)
VES_EVENT_DATA = \
{
    "event": {
        "commonEventHeader": {
            "sourceId": "VDF-site-5GC03 ",
            "startEpochMicrosec": 1597542547667,
            "eventId": "321248568971899999",
            "nfcNamingCode": " ",
            "reportingEntityId": "amzn1.ask.device.AFUMVDLEOWA7YALIFYIICR2E6MJ3PFZIOZ5Z4LUH72OH3CDIEIZUWWBE7DUUILCA2N5 LUFR2O6CDXQY4MARJHS7QFJMBC4ZOAAGB3MS3MQRNVMWDPVFCC5I4BYYSLXUHC2MJBKLJK3O7U4ZRYR777NY45CFZXUUTP5W2OT4XUODCLXVGPKDMK",
            "internalHea derFields": {
            },
            "eventType": "bandwidth_notification",
            "priority": "High",
            "version": 3,
            "reportingEntityName": "china m obile booth",
            "sequence": 960,
            "domain": "other",
            "lastEpochMicrosec": 1547542547667,
            "eventName": "quality",
            "sourceName": "china mobile booth",
            "nfNamin gCode": ""
        },
        "faultFields": {
            "eventSeverity": "CRITICAL",
            "alarmCondition": "The bandwidth is too low or high",
            "faultFieldsVersion": 2,
            "eventCategory": " bandwidth",
            "specificProblem": "The bandwidth is too low or high",
            "alarmInterfaceA": "VNF_194.15.13.138",
            "alarmAdditionalInformation": [
                {
                    "name": "bandwidth",
                    "value": "bandwidthVal"
                },
                {
                    "name": "timestamp",
                    "value": "timestampVal"
                }
            ],
            "eventSourceType": "HuaweiBandwidth",
            "vfStatus": "Active"
        }
    }
}

while True:
    for x in range(len(j)):
        temp1 = i[x]
        temp2 = j[x]
        temp = VES_EVENT_DATA
        for t in temp["event"]["faultFields"]["alarmAdditionalInformation"]:
            if t["name"] == "timestamp":
                t["value"] = str(temp1)
            if t["name"] == "bandwidth":
                t["value"] = str(temp2)
        onap_ves_url = "http://159.138.56.28:30345/eventListener/v5"
        http = url.PoolManager()
        data = json.dumps(temp).encode('utf-8')
        request = http.request('POST', onap_ves_url,
                               body=data,
                               headers={'Content-Type': 'application/json'})
        print(request)
        print("success")
        time.sleep(0.1)
    break