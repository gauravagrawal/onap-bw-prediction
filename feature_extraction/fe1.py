import numpy as np

#Creating Dataset
def create_dataset(dataset, lookahead):
        dataX, dataY = [], []
        for i in range(len(dataset) - lookahead):
            a = dataset[i:(i+lookahead)]
            dataX.append(a)
            dataY.append(dataset[i+lookahead])
        return np.array(dataX), np.array(dataY)