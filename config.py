#Values

# The value of the lookahead should be even
lookahead=100

train_size = 0.6
val_size = 0.2
file_name = 'lte_pattern.csv'
ptn = 'pattern'
graph_required = True
need_graph_in_training = False
training_required = True
future_value = 60
model_generated_folder = 'model_generated'
model_building_folder = 'model_building'
feature_extraction_folder = 'feature_extraction'
