from collections import defaultdict
import numpy as np
import pandas as pd
import config
import os
import importlib
from sklearn.preprocessing import MinMaxScaler

def main1(raw_data, future_value = 1):
    scaler = MinMaxScaler()
    data = scaler.fit_transform(raw_data.reshape(-1, 1)).reshape(-1)
    fe_path = [os.path.join(f[:-3]) for f in os.listdir(config.feature_extraction_folder
                                                        ) if(f[-3:] == '.py')]
    mb_path = [os.path.join(f[:-3]) for f in os.listdir(config.model_building_folder
                                                        ) if(f[-3:] == '.py')]
    error = defaultdict(lambda: defaultdict(str))
    min = float("inf")
    eval_file_name = config.model_generated_folder+'/evaluated.csv'
    eval_meta_data = pd.read_csv(eval_file_name, delimiter=',')

    if(config.training_required or not((eval_meta_data['file_name']!=config.file_name).any())):
        if((eval_meta_data['file_name']==config.file_name).any()):
            eval_meta_data = eval_meta_data[eval_meta_data.file_name != config.file_name]
        for fe in fe_path:
            globals()[fe] = importlib.import_module(config.feature_extraction_folder+"."+fe)
            x, y = globals()[fe].create_dataset(data, lookahead=config.lookahead)
            for mb in mb_path:
                globals()[mb] = importlib.import_module(config.model_building_folder+"."+mb)
                error[fe][mb] = globals()[mb].model(x, y, data)
                if min > error[fe][mb]:
                    min = error[fe][mb]
                    optimal_fe = fe
                    optimal_mb = mb
        temp_data = pd.DataFrame({"file_name": config.file_name,
                                  "optimal_fe": optimal_fe, "optimal_mb": optimal_mb,
                                  "error":error[optimal_fe][optimal_mb]}, index=[0])
        print(optimal_mb)
        eval_meta_data = pd.concat((eval_meta_data, temp_data), sort=True).set_index("file_name")
        eval_meta_data.to_csv(eval_file_name)
    else:
        prep_data = eval_meta_data[eval_meta_data['file_name'] == config.file_name]
        optimal_fe = np.array((prep_data["optimal_fe"]))[0]
        optimal_mb = np.array((prep_data["optimal_mb"]))[0]
        globals()[optimal_fe] = importlib.\
            import_module(config.feature_extraction_folder + "." + optimal_fe)
        globals()[optimal_mb] = importlib.\
            import_module(config.model_building_folder + "." + optimal_mb)

    predicted_value = globals()[optimal_mb].get_value(data, future_value)
    predicted_value = scaler.inverse_transform(predicted_value.reshape(-1,1)).reshape(-1)
    return predicted_value, optimal_mb